#!/usr/bin/env pythoh3

'''
Busca una palabra en una lista de palabras
'''
import sys

import sortwords

def search_word(word, words_list: list):

    notin = True
    for i in words_list:
        if i.lower() == word.lower():
            position = words_list.index(i)
            words_list[words_list.index(i)]= 0
            notin = False
    if notin:
        raise Exception
    else:
        return position

def main():
    if len(sys.argv[1:]) >= 2:
        word_list: list = sys.argv[2:]
        ordered_list = sortwords.sort(word_list)
        inlist = False
        for i in ordered_list:
            if i.lower() == sys.argv[1].lower():
                inlist = True
                break
        if not inlist:
            raise SystemExit

        sortwords.show(ordered_list)
        position = search_word(sys.argv[1], ordered_list)
        print(position)
    else:
        raise SystemExit


if __name__ == '__main__':
    main()
